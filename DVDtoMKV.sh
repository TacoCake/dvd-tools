#!/bin/bash
# Converts:
#  - DVD
#  - AUDIODVD
# into x264 mkv whilst conserving:
#  - chapters
#  - Chunk order
#  - all audio tracks.

## Dependencies
# ffmpeg (Converting, concatenating, extracting Video, Frames, Audio)
# lsdvd (Extract chapters)
# ffprobe (Get number of audio tracks)
# sox (Concatenate audio files)
# mkvmerge (Merge mkv with chapters)

DEP_ERROR=0

if [[ "$(which lsdvd 2>/dev/null || echo FALSE)" == "FALSE" ]];
then
    echo "Can't find lsdvd"
    DEP_ERROR=1
fi

if [[ "$(which ffmpeg 2>/dev/null || echo FALSE)" == "FALSE" ]];
then
    echo "Can't find ffmpeg"
    DEP_ERROR=1
fi

if [[ "$(which ffprobe 2>/dev/null || echo FALSE)" == "FALSE" ]];
then
    echo "Can't find ffprobe"
    DEP_ERROR=1
fi

if [[ "$(which sox 2>/dev/null || echo FALSE)" == "FALSE" ]];
then
    echo "Can't find sox"
    DEP_ERROR=1
fi

if [[ "$(which mkvmerge 2>/dev/null || echo FALSE)" == "FALSE" ]];
then
    echo "Can't find mkvmerge"
    DEP_ERROR=1
fi

if [[ $DEP_ERROR -eq 1 ]];
then
    exit 1
fi

if [[ "$1" == "" ]];
then
    echo "$(tput setaf 1)Error! Missing dvd path argument."
    echo ""
    echo "Correct script usage DVDtoMKV.sh \"/path/to/your/dvd/folder\""
    exit 2
fi

TYPE="video"
IS_ISO=0

# Check if ISO, if so, mount ISO before
if [[ "$(file --mime-type -b "$1")" == "application/x-iso9660-image" ]];
then
    tput sgr0
    echo "Input is an ISO file."
    echo "Mounting to temporary path..."
    tput setaf 3

    mkdir "$1_mount"
    sudo mount "$1" "$1_mount" -o loop

    IS_ISO=1

    tput sgr0
    echo "Extract to processing path..."
    tput setaf 3

    mkdir "$1_process"
    cp -r "$1_mount"/* "$1_process"
    sudo umount "$1_mount"
    rm -r "$1_mount"

    TMP="$1_process"
    set "$TMP"
    cd "$1"
else
  cd "$1"
fi

name=${PWD##*/}
# Formats seconds (0.000) into an HH:MM:SS.NNN format
format (){
    input=$1
    rawSeconds=${input%%.*}

    # Hours
    hours=$(($rawSeconds/60/60))
    # Minutes
    minutes=$(($rawSeconds/60))
    minutes=$(($minutes - $hours * 60))
    # Seconds
    seconds=$(($rawSeconds - ($hours * 60 * 60) - ($minutes * 60)))
    # Nano
    nano=${input:(-3)}

    # Pad with zeros to get a clean HH:MM:SS.NNN format
    hours=$(printf %02d $hours)
    minutes=$(printf %02d $minutes)
    seconds=$(printf %02d $seconds)

    # Add everything together
    echo "$hours:$minutes:$seconds.$nano"
}

# Extract chapters using lsdvd and save them in a format mkvmerge can read
# TODO extract chapters' names
chaptersToFile () {
        tput sgr0
        echo "Creating chapter file"
        tput setaf 3
        chapters=$(lsdvd . -cq | awk '/Chapter:/ {print $4}' | awk '{print substr($1, 1, length($1)-1)}')
        chapterid=2
        lastC="0.000"
        rm -f chapters.txt
        touch chapters.txt
        echo "CHAPTER"$(printf %02d 1)"=00:00:00.000" >> chapters.txt
        echo "CHAPTER01NAME=1" >> chapters.txt

        for c in $chapters; do
            hoursC=$(date --date=$c +"%H")
            minutesC=$(date --date=$c +"%M")
            secondsC=$(date --date=$c +"%S.%3N")
            c=$(echo "($hoursC*60*60) + ($minutesC*60)+$secondsC" | bc)

            lastC=$(echo "$lastC+$c" | bc)
            formattedC=$(format $lastC)

            echo "CHAPTER"$(printf %02d $chapterid)"=$formattedC" >> chapters.txt
            echo "CHAPTER"$(printf %02d $chapterid)"NAME=$chapterid" >> chapters.txt
            let "chapterid=chapterid+1"
        done
}

convert () {
    # If contains AUDIODVD directory then asume audio format
    if [[ -d "AUDIODVD" ]];
    then
        cd "AUDIODVD"
        TYPE="audio"
    fi
    if [[ -d "AudioDVD" ]];
    then
        cd "AudioDVD"
        TYPE="audio"
    fi

    if [[ -d "VIDEO_TS" ]];
    then
        tput sgr0
        echo "Type: $TYPE"
        echo "Contains: VIDEO_TS"
        tput setaf 3
        chaptersToFile

        METAHEAD=$(head VIDEO_TS/VIDEO_TS.IFO)

        # If dvd meta info contains AUDIODVD, then it is an audio format DVD
        if [[ "$METAHEAD" == *"AUDIODVD"* ]];
        then
            TYPE="audio"
        fi

        TOTALSTEPS=0

        # If audio format, extract frames split audio into tracks based on chapters, then assign those frames to their respective audio tracks, and concatenate the result into one big mkv file with chapters.
        if [[ "$TYPE" == "audio" ]];
        then
            TOTALSTEPS=10 # Indicates the number of total steps to convert
            CURRENTSTEP=1 # Indicates the current step
            tput sgr0
            echo "Type: Audio"
            # Calculate which audio tracks to extract
            echo "$CURRENTSTEP/$TOTALSTEPS Calculating which audio tracks to extract"
            tput setaf 3
            highestAudioTrackCounts=0

            for f in VIDEO_TS/VTS*.VOB; do
                FP=$(ffprobe -v error -select_streams a -show_entries stream=index -of csv=p=0 "$f")
                numberOfAudioTracks=0
                for t in $FP;do
                    let "numberOfAudioTracks=numberOfAudioTracks+1"
                done
                if [[ $numberOfAudioTracks -gt $highestAudioTrackCounts ]];
                then
                    highestAudioTrackCounts=$numberOfAudioTracks
                fi
            done
            let "CURRENTSTEP=CURRENTSTEP+1"

            # extract audio tracks
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Extracting audio tracks"
            tput setaf 3
            if [[ ! -f "audioFull_done" ]];
            then
                rm -f audioPart*.mka
                fileid=0
                #for each file, check audio track counts, if lower than highest, don't concat it
                for f in VIDEO_TS/VTS*.VOB; do
                    # Count number of audio tracks
                    FP=$(ffprobe -v error -select_streams a -show_entries stream=index -of csv=p=0 "$f")
                    numberOfAudioTracks=0
                    for t in $FP;do
                        let "numberOfAudioTracks=numberOfAudioTracks+1"
                    done
                    if [[ $numberOfAudioTracks -ge $highestAudioTrackCounts ]];
                    then
                        # if has correct number of audio tracks add to concat list
                        n=`printf %05d $fileid`
                        ffmpeg -hide_banner -v quiet -stats -i "$f" -c:a copy -vn -sn -map 0:a -fflags +genpts -n audioPart$n.mka || (rm -f "audioPart$n.mka";exit 1)
                        let "fileid=fileid+1"
                    fi
                done
            fi
            let "CURRENTSTEP=CURRENTSTEP+1"

            # Concatenate audio
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Concatenating audio tracks"
            tput setaf 3
            if [[ ! -f "audioFull_done" ]];
            then
                rm -f audioFull.mka
                find audioPart*.mka | sed 's:\ :\\\ :g'| sed 's/^/file /' > fl_a.txt && ffmpeg -v quiet -stats -f concat -i fl_a.txt -c copy -map 0 audioFull.mka || (rm -f audioFull.mka; exit 1)
                touch "audioFull_done"
            fi
            let "CURRENTSTEP=CURRENTSTEP+1"

            # Split audio into songs
            mkdir -p audio_chunks
            rm -f list.csv

            # Get chapters
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Getting chapters..."
            tput setaf 3
            chapters=$(lsdvd . -cq | awk '/Chapter:/ {print $4}' | awk '{print substr($1, 1, length($1)-1)}')
            id=0
            lastC="0.000"
            segmentTimesBuffered=""
            segmentTimes=""
            for c in $chapters; do
                hoursC=$(date --date=$c +"%H")
                minutesC=$(date --date=$c +"%M")
                secondsC=$(date --date=$c +"%S.%3N")
                c=$(echo "($hoursC*60*60) + ($minutesC*60)+$secondsC" | bc)
                #echo "Start at: " $(format $start) " to " $(format $lastC)

                currentC=$(echo "$lastC+$c" | bc)

                if [[ ${#c} -le 4 ]];
                then
                    echo "Segment too short ($c), skipping"
                    # Remove last chapter
                    segmentTimes=$segmentTimesBuffered

                    # add current
                    segmentTimes=$segmentTimes","
                    segmentTimes=$segmentTimes"$currentC"

                    lastC=$currentC
                    continue
                fi

                segmentTimesBuffered=$segmentTimes

                if [[ id -gt 0 ]];
                then
                    segmentTimes=$segmentTimes","
                fi
                segmentTimes=$segmentTimes"$currentC"
                lastC=$currentC
                let "id=id+1"
            done

            # Remove last segment to avoid a useless chunk at the end
            segmentTimes=$segmentTimesBuffered
            let "CURRENTSTEP=CURRENTSTEP+1"

            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Creating audiochunks..."
            tput setaf 3
            # Create audio chunks
            if [[ ! -d "frames" ]];
            then
                echo $segmentTimes
                ffmpeg -v quiet -stats -i "audioFull.mka" -segment_list list.csv -c copy -f segment -segment_times $segmentTimes audio_chunks/chunk%d.mka || (rm -rf "audio_chunks"; exit 1)
            fi
            let "CURRENTSTEP=CURRENTSTEP+1"

            # Get frames
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Extracting frames..."
            tput setaf 3
            if [[ ! -d "final_chunks" ]];
            then
                mkdir -p frames
                echo "  Concatenating VOBs"
                # Concat vobs
                vobid=0
                toCat=""
                for f in VIDEO_TS/VTS*.VOB; do
                    n=`printf %02d $vobid`
                    if [[ vobid -eq 0 ]];then
                        let "vobid=vobid+1"
                        continue
                    fi
                    toCat=$toCat" $f"
                done
                cat $toCat > concat.VOB
                tput sgr0
                echo "  Extract frames from concatenated VOBs"
                tput setaf 3

                # Extract frames from concatenated vob
                # NOTE: there seems to be a weird bug in FFMPEG when converting frames to JPEG. The more frames extracted to lower the quality gets. So it is preferable to keep using BMP files instead to avoid quality loss
                ffmpeg -hide_banner -v quiet -stats -i concat.VOB -y "frames/%05d.bmp" || (rm -rf "frames"; exit 1)
                rm -f concat.VOB
            fi
            let "CURRENTSTEP=CURRENTSTEP+1"

            numFrames=$(ls -1q frames/*.bmp | wc -l)
            numAudioChunks=$(ls -1q audio_chunks/*.mka | wc -l)

            if [[ $numFrames -ne $numAudioChunks ]];
            then
                echo ""
                echo "[!!WARNING!!] Number of audio chunks doesn't match number of frames !"
                echo ""
            fi

            #Apply frames to segments
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Making final chunks..."
            tput setaf 3

            if [[ ! -f "concat.mkv" ]];
            then
                #Apply frames to segments
                mkdir -p final_chunks

                max=$id
                chunkid=0
                posterid=0
                for z in audio_chunks/chunk*.mka;do
                    audioChunk="audio_chunks/chunk$chunkid.mka"

                    framepath=""

                    x=0
                    for frame in frames/*bmp; do
                    if [[ $posterid -eq $x ]];
                    then
                        framepath=$frame
                        break
                    fi
                    let "x=x+1"
                    done

                    n=`printf %05d $chunkid`

                    ffmpeg -hide_banner -v quiet -stats -loop 1 -i "$framepath" -i "$audioChunk" -shortest -c:a copy -c:v libx264 -tune stillimage -pix_fmt yuv420p -n "final_chunks/$n.mkv" || (rm -rf "final_chunks"; exit 1)

                    let "posterid=posterid+1"
                    let "chunkid=chunkid+1"
                    echo $posterid
                done
            fi
            let "CURRENTSTEP=CURRENTSTEP+1"

            #Recombine segments into one video
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Recombine everything into one file..."
            tput setaf 3

            if [[ ! -f "concat.mkv" ]];
            then
                find final_chunks/*.mkv | sed 's:\ :\\\ :g'| sed 's/^/file /' > fl.txt && ffmpeg -v quiet -stats -f concat -i fl.txt -c copy -map 0 concat.mkv || (rm -f fl.txt; exit 1)
                rm -f fl.txt
            fi
            let "CURRENTSTEP=CURRENTSTEP+1"

            #Apply chapters to final
            tput sgr0
            echo "$CURRENTSTEP/$TOTALSTEPS Apply chapters to file..."
            tput setaf 3
            mkvmerge concat.mkv  --chapters 'chapters.txt' -o "$1/$name.mkv" || exit 1
            let "CURRENTSTEP=CURRENTSTEP+1"

            if [[ -f "$1/$name.mkv" ]];
            then
                tput sgr0
                echo "$CURRENTSTEP/$TOTALSTEPS Cleaning up"
                tput setaf 3
                rm -rf audio_chunks
                rm -rf final_chunks
                rm -rf frames
                rm -f concat.mkv
                rm -f audio*.mka
                rm -f audioFull_done
                rm -f list.csv
                echo "$(tput setaf 2)Done converting!"
            else
            echo "$(tput setaf 1)Failed to convert!"
            fi
        else
            echo "Converting files"
            mkdir temp
            if [[ ! -f "temp/convert_done" ]];
            then
                cd "VIDEO_TS"
                for file in VTS*.VOB; do
                    # TODO extract VOBSUB subtitles

                    # check for duration, if less than 10 seconds, don't convert it
                    DURATION=$(ffprobe -v quiet -of csv=p=0 -show_entries format=duration "$file")

                    if [[ "$(echo "$DURATION < 10.0000" | bc)" -eq 1 ]]; then
                        echo "This file is too small! $file"
                        continue
                    fi
                    echo "    Converting file $file"
                    # Tries to Convert VOB files into multiple MKVs, to try and seperate VIDEO Menus from the actual video
                    # if it initially fails, try to extract only the audio
                    ffmpeg -hide_banner -v quiet -stats -i "$file" -c:a copy -c:s copy -c:v libx264 -qp 20 -map 0:v? -map 0:a? -map 0:s? -y -fflags +genpts "../temp/${file%%.*}.mkv" || ffmpeg -hide_banner -v quiet -stats -i "$file" -c:a copy -c:s copy -vn -map 0:a? -map 0:s? -y -fflags +genpts "../temp/${file%%.*}.mkv"
                done
                touch "../temp/convert_done"
                cd ..
            fi

            # Get highestaudio track counts
            highestAudioTrackCounts=0

            for f in temp/*.mkv; do
                FP=$(ffprobe -v error -select_streams a -show_entries stream=index -of csv=p=0 "$f")
                num=0
                for t in $FP;do
                    let "num=num+1"
                done
                if [[ num -gt highestAudioTrackCounts ]];
                then
                    highestAudioTrackCounts=$num
                fi
            done

            rm fl.txt
            touch fl.txt
            #for each file, check audio track counts, if lower than highest, don't concatenate it
            for f in temp/*.mkv; do

                FP=$(ffprobe -v error -select_streams a -show_entries stream=index -of csv=p=0 "$f")
                num=0
                for t in $FP;do
                    let "num=num+1"
                done
                if [[ num -ge highestAudioTrackCounts ]];
                then
                    # if has correct num of audio tracks add to concat list
                    echo "file $f" >> fl.txt
                fi
            done


            echo "Concatenating files"
            if [[ ! -f "temp/concat_done" ]];
            then
                ffmpeg -hide_banner -v quiet -stats -f concat -i fl.txt -c copy -map 0 -fflags +genpts temp/concat.mkv; rm fl.txt
                touch "temp/concat_done"
            fi

            if [[ "$(lsdvd . -cq | awk '/Title:/ {print $1$2}' | awk 'NR<=2{next} {print $1}')" -eq "" ]];
            then
                tput sgr0
                echo "Apply chapters from IFO"
                tput setaf 3
                #Apply chapters to final
                mkvmerge temp/concat.mkv  --chapters 'VIDEO_TS/VIDEO_TS.IFO' -o "$1/$name.mkv"
            else
                tput sgr0
                echo "Apply chapters from file"
                tput setaf 3

                #Apply chapters to final
                mkvmerge temp/concat.mkv  --chapters 'chapters.txt' -o "$1/$name.mkv"
            fi

            #if no chapters, check if multiple chapters for title 1
            #apply those chapters sequentially

            if [[ -f "$1/$name.mkv" ]];
            then
                echo "Cleaning up"
                rm -rf temp
                rm -rf chapters.txt
                rm -rf fl.txt
                echo "$(tput setaf 2)Done converting!"
            fi
        fi
    fi
}
if [[ -d "Disk 1" ]];then
    numDisk=1
    original_name=$name
    tput sgr0
    echo "Converting Disk 1"
    tput setaf 3
    cd "Disk 1"
    name="disk1"
    convert "$1"
    cd ..
    if [[ -d "Disk 2" ]];then
        numDisk=2
        tput sgr0
        echo "Converting Disk 2"
        tput setaf 3
        cd "Disk 2"
        name="disk2"
        convert "$1"
        cd ..
    fi
    # concatenante disks
    if [[ $numDisk -gt 1 ]];then
        mkvmerge -o "$original_name.mkv" disk1.mkv + disk2.mkv
        rm disk*.mkv
    else
        mv "disk1.mkv" "$original_name.mkv"
    fi

    # apply chapters
else
    convert "$1"
fi
