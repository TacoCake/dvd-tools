# DVD Tools

This is a collection of scripts I've made to facilitate interacting with VIDEO_TS folders.

## List of scripts and their use

| Script | Use | Usage | Dependencies
| - | - | - | - |
| DVDtoMKV.sh | Tries to convert AUDIODVDs & DVDs to MKV whilst trying to conserve: Chapters, multiple Audiotracks, order. | `DVDtoMKV.sh <Path to Root folder of DVD containing AUDIODVD/VIDEO_TS>`| ffmpeg lsdvd ffprobe sox |
 
## Roadmap
- Compatibility
- - [X] AUDIODVD
- - [X] DVD
- - [ ] Audio CD
- - [ ] BDMV - [References](https://en.wikipedia.org/wiki/Blu-ray#Directory_and_file_structure)
- - - [ ] 3D BDMV [R1](https://github.com/Vargol/h264-tools),[R2](https://github.com/Vargol/h264-tools/wiki/Conversion-script-for-MVC-3D-blu-ray-extracted-by--MakeMKV)
- - [ ] BDAV
- [ ] GUI application
